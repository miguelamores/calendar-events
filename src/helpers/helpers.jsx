export const mapRemindersToDay = (reminder, month, weather) => {
  let currentDay = month.find(item => item.id === reminder.dayId);
  currentDay.weather = mapWeatherToDay(currentDay, weather);
  currentDay.reminders.push(reminder);
  sortReminderByTime(currentDay.reminders);
  currentDay.background = reminder.background;
  month.fill(currentDay, currentDay.id, currentDay.id);
  return month;
};

export const mapRemindersToEdit = (currentReminder, month) => {
  let currentDay = month.find(item => item.id === currentReminder.dayId);
  const findedReminder = currentDay.reminders.findIndex(
    reminder => reminder.id === currentReminder.id
  );
  currentDay.reminders[findedReminder] = currentReminder;
  sortReminderByTime(currentDay.reminders);
  currentDay.background = currentReminder.background;
  month.fill(currentDay, currentDay.id, currentDay.id);
  return month;
};

export const deleteReminder = (currentReminder, month) => {
  let currentDay = month.find(item => item.id === currentReminder.dayId);
  const findedReminder = currentDay.reminders.findIndex(
    reminder => reminder.id === currentReminder.id
  );
  currentDay.reminders.splice(findedReminder, 1);
  currentDay.background = "#fff";
  month.fill(currentDay, currentDay.id, currentDay.id);
  return month;
};

const sortReminderByTime = remiders => {
  remiders.sort((a, b) => {
    if (a.time > b.time) {
      return 1;
    }
    if (a.time < b.time) {
      return -1;
    }
    return 0;
  });
};

const mapWeatherToDay = (currentDay, weather) => {
  const dayWeather = weather.list[currentDay.id].weather[0].main;
  return dayWeather;
};
