export const ADD_MONTH = "ADD_MONTH";
export const ADD_REMINDER = "ADD_REMINDER";
export const EDIT_REMINDER = "EDIT_REMINDER";
export const DELETE_REMINDER = "DELETE_REMINDER";
export const GET_WEATHER = "GET_WEATHER";

export const addCalendar = (dispatch, payload) => {
  dispatch({ type: ADD_MONTH, payload });
};

export const addReminder = (dispatch, payload) => {
  dispatch({ type: ADD_REMINDER, payload });
};

export const editReminder = (dispatch, payload) => {
  dispatch({ type: EDIT_REMINDER, payload });
};

export const deleteReminder = (dispatch, payload) => {
  dispatch({ type: DELETE_REMINDER, payload });
};

export const getWeather = dispatch => {
  fetch(
    "http://api.openweathermap.org/data/2.5/forecast?q=Quito&appid=d17878781d1b7ba158a0623054c6f28d&cnt=100",
    {
      method: "GET"
    }
  )
    .then(res => res.json())
    .then(res => dispatch({ type: GET_WEATHER, payload: res }));
};
