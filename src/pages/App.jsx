import React, { useEffect, useState } from "react";
import {
  addCalendar,
  addReminder,
  editReminder,
  getWeather,
  deleteReminder
} from "../actions/calendar";
import "../sass/styles.scss";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import ReminderDialog from "../components/ReminderDialog/ReminderDialog";
import StyledDay from "../components/Day/StyledDay";
import DayHeader from "../components/DayHeader/DayHeader";

import { weekDays } from "../helpers/constants";

const App = () => {
  const calendar = useSelector(state => state.calendar);
  const dispatch = useDispatch();
  const [openDialog, setOpenDialog] = useState(false);
  const [selectedDay, setSelectedDay] = useState();
  const [currentReminder, setCurrentReminder] = useState();

  useEffect(() => {
    initCalendar();
    getWeather(dispatch);
  }, []);

  const initCalendar = () => {
    const startWeek = moment()
      .startOf("month")
      .week();
    const currentMonth = Array(35)
      .fill(0)
      .map((n, i) => {
        let day = moment()
          .week(startWeek)
          .startOf("week")
          .clone()
          .add(n + i, "day");
        return {
          id: i,
          day: day.format("D"),
          textDay: day.format("dddd"),
          weather: null,
          background: null,
          reminders: []
        };
      });
    addCalendar(dispatch, currentMonth);
  };

  const handleOpenDialog = dayId => {
    setCurrentReminder();
    setSelectedDay(dayId);
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const handleCreateReminder = reminder => {
    addReminder(dispatch, reminder);
  };

  const handleSaveReminder = reminder => {
    editReminder(dispatch, reminder);
  };

  const handleEditReminder = (index, reminder) => {
    setCurrentReminder(reminder);
    setOpenDialog(true);
  };

  const handleDeleteReminder = reminder => {
    deleteReminder(dispatch, reminder);
    handleClose();
  };

  return (
    <div className="Calendar__container">
      <div className="Calendar">
        {weekDays.map(day => (
          <DayHeader key={day} text={day} />
        ))}

        {calendar &&
          calendar.month &&
          calendar.month.map((day, index) => (
            <StyledDay
              key={index}
              isWeekend={
                day.textDay === weekDays[0] || day.textDay === weekDays[6]
              }
              background={day.background}
            >
              <div
                className="StyledDay__day"
                onClick={() => handleOpenDialog(index)}
              >
                {day.day}
              </div>
              {day.reminders &&
                day.reminders.map((reminder, index) => (
                  <span
                    onClick={() => handleEditReminder(index, reminder)}
                    key={`${reminder.dayId}${reminder.note}`}
                    className="StyledDay__reminder"
                  >
                    {reminder.time} -- {reminder.note} -- {day.weather}
                  </span>
                ))}
            </StyledDay>
          ))}

        <ReminderDialog
          selectedDay={selectedDay}
          handleCreateReminder={handleCreateReminder}
          handleSaveReminder={handleSaveReminder}
          openDialog={openDialog}
          handleClose={handleClose}
          currentReminder={currentReminder}
          handleDeleteReminder={handleDeleteReminder}
        />
      </div>
    </div>
  );
};

export default App;
