import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import calendar from "../reducers/calendar";

const rootReducer = combineReducers({
  calendar
});

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;
