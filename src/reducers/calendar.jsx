import {
  ADD_MONTH,
  ADD_REMINDER,
  EDIT_REMINDER,
  GET_WEATHER,
  DELETE_REMINDER
} from "../actions/calendar";
import {
  mapRemindersToDay,
  mapRemindersToEdit,
  deleteReminder
} from "../helpers/helpers";

const initialState = {
  month: "",
  reminder: null,
  weather: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_MONTH:
      return { ...state, month: action.payload };
    case ADD_REMINDER:
      return {
        ...state,
        month: mapRemindersToDay(action.payload, state.month, state.weather)
      };
    case EDIT_REMINDER:
      return {
        ...state,
        month: mapRemindersToEdit(action.payload, state.month)
      };
    case DELETE_REMINDER:
      return { ...state, month: deleteReminder(action.payload, state.month) };
    case GET_WEATHER:
      return {
        ...state,
        weather: action.payload
      };
    default:
      return state;
  }
};
