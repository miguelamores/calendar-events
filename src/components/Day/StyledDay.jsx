import styled from "styled-components";

const StyledDay = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #000;
  padding: 1rem 1.5rem;
  font-weight: bold;
  color: ${props => (props.isWeekend ? "#3372A6" : "#000")};
  background-color: ${props =>
    !!props.background
      ? props.background
      : props.isWeekend
      ? "#F1F1F1"
      : "#fff"};

  .StyledDay {
    &__day {
      cursor: pointer;
      font-size: 1.2rem;
    }

    &__reminder {
      display: flex;
      justify-content: flex-start;
      color: yellowgreen;
      font-weight: bold;
      cursor: pointer;
    }
  }
`;

export default StyledDay;
