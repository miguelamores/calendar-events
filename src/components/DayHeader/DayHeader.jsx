import React from "react";

const Day = ({ text }) => {
  return <div className="DayHeader">{text}</div>;
};

export default Day;
