import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { HuePicker } from "react-color";

const initialState = { note: "", time: "07:30", city: "", background: "#fff" };

const ReminderDialog = props => {
  const [reminder, setReminder] = useState(initialState);
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    if (!!props.currentReminder) {
      setIsEdit(true);
      setReminder(props.currentReminder);
    } else {
      setIsEdit(false);
      setReminder({ ...initialState, dayId: props.selectedDay });
    }
  }, [props.currentReminder, props.selectedDay]);

  const handleChange = e => {
    let newReminder = { ...reminder };
    newReminder[e.target.id] = e.target.value;
    setReminder(newReminder);
  };

  const handleChageColor = e => {
    let newReminder = { ...reminder };
    newReminder.background = e.hex;
    setReminder(newReminder);
  };

  const handleSave = () => {
    isEdit
      ? props.handleSaveReminder(reminder)
      : props.handleCreateReminder({ ...reminder, id: generateId() });
    props.handleClose();
    setReminder({ ...initialState, dayId: props.selectedDay });
  };

  const generateId = () => {
    return (
      Date.now().toString(36) +
      Math.random()
        .toString(36)
        .substr(2, 5)
    ).toUpperCase();
  };

  const handleDeleteReminder = () => {
    props.handleDeleteReminder(reminder);
  };

  return (
    <Dialog
      open={props.openDialog}
      onClose={props.handleClose}
      aria-labelledby="form-dialog-title"
      data-testid="dialog"
    >
      <span className="reminder__title" id="form-dialog-title">
        {isEdit ? "Edit" : "New"} reminder
      </span>
      <DialogContent className="reminder">
        <TextField
          value={reminder.note}
          autoFocus
          margin="dense"
          id="note"
          label="Note:"
          type="text"
          fullWidth
          inputProps={{ maxLength: 30 }}
          onChange={handleChange}
        />
        <TextField
          value={reminder.time}
          id="time"
          label="Time"
          type="time"
          onChange={handleChange}
        />
        <TextField
          value={reminder.city}
          id="city"
          label="City"
          type="text"
          onChange={handleChange}
        />
        <HuePicker color={reminder.background} onChange={handleChageColor} />
      </DialogContent>
      <DialogActions>
        {isEdit && (
          <Button onClick={handleDeleteReminder} color="primary">
            Delete
          </Button>
        )}
        <Button onClick={props.handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={handleSave} color="primary" data-testid="create">
          {isEdit ? "Save" : "Create"}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ReminderDialog;
