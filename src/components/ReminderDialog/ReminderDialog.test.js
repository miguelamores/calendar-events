import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ReminderDialog from "./ReminderDialog";

test("Create new reminder", () => {
  const handleCreateReminder = jest.fn();
  const handleClose = jest.fn();
  const { getByTestId } = render(
    <ReminderDialog
      openDialog={true}
      handleClose={handleClose}
      handleCreateReminder={handleCreateReminder}
      selectedDay="1"
    />
  );
  fireEvent.click(getByTestId("create"));
  expect(handleCreateReminder).toBeCalledWith(
    expect.objectContaining({
      background: expect.any(String),
      city: expect.any(String),
      dayId: expect.any(String),
      id: expect.any(String),
      note: expect.any(String),
      time: expect.any(String)
    })
  );
});
