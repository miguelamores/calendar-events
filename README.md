## Add reminders to calendar

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test to create a new reminder

## Instructions

When you start the project you will see a calendar.<br />
To add a new reminder you have to click in the day number.<br />
Then a Dialog will show up and you should fill the fields and press the create button.<br />
To eddit a reminder you can select it and a Dialog will show up. Also you have a button to delete the selected reminder.

## Project structure

In src folder you will see the next structure folders:

- actions: Actions to dispatch to the reducer.
- components: Components used into the application, includes functional components and styled components
- helpers: Constant file and a helper file with fuctions to use into the reducer
- pages: Main page to render the calendar.
- reducers: The reducer used in the application.
- redux: Configuration file of redux and store.
- sass: Folder containing the css of the application.
